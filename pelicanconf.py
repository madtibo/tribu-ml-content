#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'madtibo'
BIO = u'Grand fan de Totoro. DBA Dev chez GitGuardian'
SITENAME = u'Totoro chez les DBA'
SITESUBTITLE = u'DBAのトトロ'
SITEURL = 'https://blog.tribu-ml.fr/pelican'
PROFILE_IMAGE = "tete_a_totoro.png"

FAVICON = "favicon.ico"
FAVICON_TYPE = "png"

PLUGIN_PATHS = ["/usr/local/lib/python2.7/dist-packages/pelican/plugins"]
PLUGINS = ['sub_parts']

PATH = 'content'
STATIC_PATHS = ['images', 'extra/robots.txt', 'extra/favicon.ico']
EXTRA_PATH_METADATA = {
    'extra/robots.txt': {'path': 'robots.txt'},
    'extra/favicon.ico': {'path': 'favicon.ico'}
}

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'fr'
SUMMARY_MAX_LENGTH = 40
 
# Feed generation is usually not desired when developing
FEED_DOMAIN = SITEURL
FEED_ALL_RSS = 'feeds/all.rss.xml'
CATEGORY_FEED_RSS = None
AUTHOR_FEED_RSS = None

# commentaires avec Disqus
DISQUS_SITENAME = 'dbanototoro'

# Blogroll
LINKS = (('トトロ', 'https://fr.wikipedia.org/wiki/Mon_voisin_Totoro'),
         ('LA base de données', 'https://www.postgresql.org/'),
         ('La plus belle des distrib\'', 'https://www.debian.org/'),
         ('Mon CV', 'https://tribu-ml.fr/cv/thibaut/cv_thibaut_madelaine.html'),
)

# Social widget
SOCIAL = ( ('email', 'thibaut_geek+blog@tribu-ml.fr'), ('gitlab', 'https://gitlab.com/madtibo')
)

DEFAULT_PAGINATION = 5
DEFAULT_ORPHANS = 2
 
# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

THEME = "pelican-hyde"
#THEME = "notmyidea"

DEFAULT_CATEGORY = 'général'

FILENAME_METADATA = '(?P<slug>(?P<date>\d{4}-\d{2}-\d{2})-[^_]+)(_(?P<lang>\w{2}))?'

MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': {
            'css_class': 'highlight',
        },
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'markdown.extensions.tables': {},
    },
    'output_format': 'html5',
}
