Title: PostgreSQL 10
Tags: PostgreSQL, 10, nouveauté

Cet article est issu de ma conférence au [PG Session 2017](https://www.postgresql-sessions.org/_media/9/thibaut-madelaine_les-nouveaute-postgresql10.pdf).  
Une vidéo est même [disponible](https://tribu-ml.fr/nextcloud/index.php/s/ik2k30imT6QVFzw).

## De très nombreuses évolutions

* le changement de numérotation
* les changements de nommages
* des améliorations de performances
* ...
 
Ancienne numérotation exprimée sur 3 nombres :

    :::
    9 . 6 . 3
    Majeure1 . Majeure2 . Mineure


Nouvelle numérotation exprimée sur 2 nombres uniquement :

    :::
    10 . 2
    Majeure . Mineure

Les renommages :
  * Au niveau des répertoires                                                   
    * `pg_xlog` -> `pg_wal`                                                     
    * `pg_clog` -> `pg_xact`                                                    
  * Au niveau des fonctions                                                     
    * `xlog` -> `wal`                                                           
    * `location` -> `lsn`                                                       
  * Au niveau des outils                                                        
    * `xlog` -> `wal`    
 

