Title: 3 - conclusion
Tags: PostgreSQL, SQLServer, SIG, benchmark
Description: Comparatif de performances de requêtes SIG en PostgreSQL et SQL Server
Authors: Cyril Chaboisseau, madtibo

Nous conclurons cet article en comparant les performances des deux moteurs sur l'ensemble des 10 requêtes. Nous évoquerons leurs capacités respectives de parallélisation et les limites de ces tests sur la comparaison des deux SGBDR.


## Temps total

Comparer le temps total pris par l'ensemble des 10 requêtes sur-représente les requêtes longues. Nous comparerons aussi le nombre de requêtes s'exécutant le plus rapidement sur un des moteurs, ici PostgreSQL, ainsi que l'évolution de ce temps total avec 1 ou 16 clients.

Voici le tableau récapitulant le temps d'exécution et si PostgreSQL est plus rapide pour chacun des tests avec un client unique :

| 1 client | SQL Server (ms) | PostgreSQL (ms) | Rapport PostgreSQL / SQL Server | PostgreSQL plus rapide |
|:--:|:-----------:|:-----------:|:-----:|:-----:|
| Q1 |    333      |    435      | 131 % |       |
| Q2 |    412      |     52      |  13 % |  X    |
| Q3 |  2 730      | 12 824      | 470 % |       |
| Q4 |  1 498      |     14      | 0,9 % |  X    |
| Q5 |  3 765      | 16 199      | 430 % |       |
| Q6 |  1 933      |    376      |  19 % |  X    |
| Q7 |    112      |     33      |  29 % |  X    |
| Q8 |    592      |    115      |  19 % |  X    |
| Q9 |  1 393      |     81      | 5,9 % |  X    |
| Q10| 14 065      | 29 467      | 210 % |       |
| **Total** | **27 secondes** | **1 minute**    |   | **6 / 10** |

Le tableau récapitulatif pour 16 clients :

| 1 client | SQL Server (ms) | PostgreSQL (ms) | Rapport PostgreSQL / SQL Server | PostgreSQL plus rapide |
|:--:|:-----------:|:-----------:|:-----:|:-----:|
| Q1 |   1 865     |   2 668     | 143 % |     |
| Q2 |   6 350     |      91     | 1,4 % |  X  |
| Q3 |  18 826     |  44 958     | 239 % |     |
| Q4 |  20 401     |      21     | 0,1 % |  X  |
| Q5 |  10 811     |  63 832     | 590 % |     |
| Q6 |   6 328     |     661     |  10 % |  X  |
| Q7 |     359     |      51     |  14 % |  X  |
| Q8 |   8 354     |     200     | 2,4 % |  X  |
| Q9 |  23 775     |     356     | 1,5 % |  X  |
| Q10| 208 518     | 102 075     |  49 % |  X  |
| **Total** | **5 minutes** |  **3,5 minutes** |   | **7 / 10**  |


En mono-client, SQL Server est 2 fois plus rapide que PostgreSQL sur l'ensemble des 10 requêtes.  
PostgreSQL est toutefois plus rapide pour 6 des requêtes sur 10.

Avec 16 clients, les rapports s'inversent et PostgreSQL est 1,4 fois plus rapide que SQL Server.  
Pour 7 des 10 requêtes, PostgreSQL est plus rapide.


## Parallélisation

| Temps total (ms) | 1 client | 16 clients | Rapport 1 à 16 clients |
|:----------------:|:--------:|:----------:|:-------:|
| SQL Server | 26 833 | 305 589 | × 11,4 |
| PostgreSQL | 59 594 | 214 921 |  × 3,6 |

Ce résultat nous montre clairement que lorsque SQL Server a des performances meilleures que PostgreSQL, c'est dans la quasi-totalité des cas parce qu'il est capable de paralléliser ses traitements. Le lancement de requêtes concurrentes en SQL Server a un effet linéaire sur le temps d'exécution des requêtes.  
Dès que le nombre de connexion concurrentes augmente, les performances de PostgreSQL deviennent meilleures que celles de SQL Server.

PostgreSQL utilise la parallélisation pour les sauvegardes en mode `directory` et pour les restaurations en mode `custom` et `directory`.  
Il est conseillé de fixer le nombre de travailleur avec l'option `-j` au nombre de processeurs. Suivant le stockage, des valeurs plus élevées peuvent encore accélérer les opérations.

Pour les requêtes PostgreSQL, la [parallélisation](https://www.postgresql.org/docs/10/static/parallel-query.html) est apparue en version *9.6*. Elle est depuis la version *10* activée par défaut. Cette parallélisation ne s'active cependant pas systématiquement.  
D'une part, toutes les opérations ne sont pas encore parallélisables. D'autre part, cette parallélisation s'appuie sur du multi-processus. Son coût de mise en place n'est pas négligeable et ne s'active que pour des opérations manipulant une quantité suffisamment importante de données.

## Somme toute, quel système doit-on préférer ?

Ces tests permettent de comparer les performances en calcul des systèmes géographiques *SQL Server Spatial* et *PostgreSQL*+*PostGIS*.  
Grâce à l'exécution de requête concurrentes, nous avons pu examiner les performances des systèmes en charge. Le volume de données chargé durant ces tests est faible et les performances sont évaluées avec un cache pré-chargé. Les capacités de gestion de cache des systèmes ne sont donc pas évaluées.

Le mode traitement analytique ou *OLAP* est caractérisé par un faible nombre de requêtes concurrentes et par un gros volume de données manipulées. SQL Server, de par sa forte capacité à paralléliser les traitements, pourrait sembler meilleur dans ce mode. Nous n'avons cependant aucune information sur les performances respectives des moteurs sur leur gestion du cache. De plus, le nombre d'opérations parallélisables augmentent à chaque nouvelle version de PostgreSQL.  
Les tests n'apportent par conséquent pas d'arguments majeur pour privilégier l'un ou l'autre des SGBDR pour une utilisation *OLAP*.

En mode traitement transactionnel en ligne ou *OLTP*, PostgreSQL montre qu'il gère beaucoup mieux la concurrence que SQL Server. 

L'article original conseillait l'utilisation de SQL Server pour du traitement de données géographiques sur Windows.  
De [nouveaux tests](https://sqlpro.developpez.com/tutoriels/comparatif-performances-sql-server-windows-linux/|deuxième article) par Frédéric Brouard montrent que les performances de SQL Server sont meilleures sur Linux que sur Windows !  
Nos tests montrent qu'en utilisation normale avec des requêtes concurrentes, le moteur libre PostgreSQL est clairement à privilégier.

Pour conclure et paraphrasant l'article original, **nous pouvons dire que PostgreSQL+PostGIS est plus rapide que SQL Server dans la manipulation des données spatiales sur Linux**.
