Title: Comparatif SIG, match retour
Tags: PostgreSQL, SQLServer, SIG, benchmark
Description: Comparatif de performances de requêtes SIG en PostgreSQL et SQL Server
Authors: Cyril Chaboisseau, madtibo

En avril 2017, le site developpez.com publiait un [article de Frédéric Brouard](https://g-ernaelsten.developpez.com/tutoriels/comparatif-sig-et-performances/). Celui-ci comparait les performances des [Système de Gestion de Base de Données](https://fr.wikipedia.org/wiki/Syst%C3%A8me_de_gestion_de_base_de_donn%C3%A9es) [PostgreSQL 9.6](https://www.postgresql.org/) et [SQL Server 2016](https://www.microsoft.com/fr-fr/sql-server/sql-server-2016) sur des requêtes d'[informations géographiques](https://fr.wikipedia.org/wiki/Syst%C3%A8me_d%27information_g%C3%A9ographique). 

Ces tests étaient réalisés sur un serveur Windows 10 version Enterprise équipé de deux processeurs Intel Xeon de 12 cœurs hyperthreadés chacun, soit 48 cœurs. SQL Server était en moyenne 10 fois plus rapide que PostgreSQL.

À la lecture de cet article, il apparaissait que les requêtes étaient parallélisées sous SQL Server mais pas sous PostgreSQL. Le serveur PostgreSQL n'ayant pas été configuré, la parallélisation apparue en version 9.6 n'y était pas activé.

Cet article soulevait deux interrogations :

  1. quel était l'impact de l'utilisation du système d'exploitation Windows sur les performances des deux SGBD ?
  2. l'activation de la parallélisation sur PostgreSQL rééquilibrerait-il le rapport entre les moteurs ?

Avec [Cyril Chaboisseau](mailto:cyril+sig@adren.org), nous avons profité de la disponibilité de [SQL Server 2017](https://www.microsoft.com/fr-fr/sql-server/sql-server-2017-linux) sur Linux pour répondre à ces 2 interrogations.

Côté PostgreSQL, nous passons à la [version 10](https://www.postgresql.org/about/news/1786/).

## SQL Server 2017 versus PostgreSQL 10 - de l’autre côté du miroir

Pour tester l'impact de la parallélisation, nous avons écrit un [script python](https://gitlab.com/madtibo/sig_comparison) permettant de lancer un certain nombre de fois la requête en parallèle. Nous pouvons ainsi analyser le comportement des deux moteurs lorsqu’ils sont fortement sollicités.

Certaines requêtes pouvaient être améliorées. Nous avons proposé un certain nombre d'optimisations.

Nous avons réalisé ces tests :

  * avec une machine plus modeste, tant pour la mémoire que pour les processeurs ;
  * sur Linux ;
  * avec des versions plus récentes : SQL Server 2017 et PostgreSQL 10 ;
  * en lançant les requêtes en parallèle ;
  * en optimisant certaines requêtes.

