Title: 1 - méthodologie
Tags: PostgreSQL, SQLServer, SIG, benchmark
Description: Comparatif de performances de requêtes SIG en PostgreSQL et SQL Server
Authors: Cyril Chaboisseau, madtibo

## Méthodologie

Merci à [Krysztophe](https://gitlab.com/Krysztophe) pour avoir d'une part prêté son serveur pour ces tests et surtout pour les nombreux commentaires sur ce texte et les évolutions sur l'[outil de bench](https://gitlab.com/madtibo/sig_comparison).

Ces benchmarks sont réalisés sur une machine Linux Ubuntu 16.04 dotée d'un noyau 4.4. L'ordinateur contient un processeur i7-6700 possédant 8 cœurs cadencés à 4 GHz et 16 Go de mémoire vive.

Les données sont stockées sur un SSD Samsung 850 EVO de 500 Go.

### Moteurs de base de données

Microsoft SQL Server est la version 14.0.3025.34 en mode _Developer_ qui correspond à la [CU6](https://support.microsoft.com/en-gb/help/4101464/cumulative-update-6-for-sql-server-2017).

La version de PostgreSQL utilisée est la 10.3 avec l'extension PostGIS 2.4.4.

Les paramétrages effectués pour PostgreSQL sont les suivants :

    :::ini
    shared_buffers = 3GB
    work_mem = 64MB
    effective_cache_size = 9GB
    maintenance_work_mem = 1GB
    min_wal_size = 1GB
    max_wal_size = 2GB
    checkpoint_completion_target = 0.9
    wal_buffers = 16MB
    random_page_cost = 0.1
    effective_io_concurrency = 200
    max_worker_processes = 7
    max_parallel_workers_per_gather = 4
    max_parallel_workers = 7


Il existe plusieurs tutoriels ou documentations permettant de savoir comment configurer une machine en fonctions des ressources et de son utilisation.  
Le site [pgtune](https://pgtune.leopard.in.ua/) permet de générer une première ébauche de fichier de configuration suivant votre serveur et vos besoins. C'est à partir de ce site que la configuration de PostgreSQL a été faite pour réaliser les tests.

### Chargement des données

Les fichiers de sauvegardes annexés à l'article original ont servi de base pour le chargement des données :

  * [_DB_GEOPG.BAK_](https://1drv.ms/u/s!AqvZfiQYoNpBgXzhXmOZd24r2Bxh) pour PostgreSQL[^pgbackup],
  * [_DB_GEO_SQLSERVER.BAK2_](https://1drv.ms/u/s!AqvZfiQYoNpBgX398-Aq8vLnon1C) pour SQLServer

[^pgbackup]: La sauvegarde PostgreSQL _DB_GEOPG.BAK_ nécessite l'extension [_pointcloud_](https://github.com/pgpointcloud/pointcloud) pour être restaurée sans erreur. Cette extension n'est pas nécessaire pour les tests. Une fois la base restaurée, sauvegardez-la et utilisez cette dernière sauvegarde pour la suite.

Les explications pour l'installation des paquets nécessaires et des scripts permettant de lancer les requêtes aux deux SGBDR sont disponibles sur le dépôt [sig_comparaison](https://gitlab.com/madtibo/sig_comparison).

### Conditions d'exécution

Les tests pour chaque moteur ont été effectués en éteignant l'autre moteur. Aucun autre traitement n'a été lancé durant le lancement du script. Nous avons l'assurance qu'aucun processus ne vient perturber les tests.

Nous testons le comportement des SGBD sous la charge en lançant les requêtes en parallèle. Un processus de test comprend le lancement initial de chargement du cache et 10 lancements successifs de la requête.

Durant les tests nous lançons ce processus de test de 1 à 16 fois en parallèle en utilisant le module python [multiprocessing](https://docs.python.org/2.7/library/multiprocessing.html?#using-a-pool-of-workers).

### Calcul des durées d'exécution

Pour les 2 SGBD, les résultats sont redirigés vers `/dev/null`.

Pour calculer les performances des requêtes, nous récupérons les durées calculées par les SGBD par les commandes `EXPLAIN ANALYSE` ou `SET STATISTICS TIME ON`.  

Nous avons utilisé la même méthode que dans l'article original. La requête est lancée une première fois pour charger le cache. Puis la requête est lancée 10 fois de suite. Les deux résultats extrêmes sont supprimés et on fait la moyenne des 8 résultats restants.

Lorsque plus d'un processus de test est lancé, nous moyennons la durée d'exécution de chaque processus de test pour obtenir la durée d'exécution de la requête.
