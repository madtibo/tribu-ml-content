Title: Sqitch ou la gestion de versions en SQL
Date: 2019-11-12
Category: PostgreSQL
Lang: fr
Status: draft

## La gestion de versions

On ne présente plus _git_ qui est maintenant LE standard de gestion de
versions. Il est utilisé dans le domaine du développement logiciel pour suivre
les évolutions apportées au code source par une multitude de personnes en
parallèle.

Dans le monde des bases de données, rien n'est prévu dans les outils de SGBD
pour gérer les évolutions de schémas. Pas de versionnage, pas de retour
arrière, pas de gestion de la modification
concurrente. [Sqitch](https://sqitch.org/) propose de changer nos façons de
faire pour prendre le meilleur de ce qui est fait dans le monde du
développement.


