un article en rest
##################

:Date: 2018-05-26
:Category: PostgreSQL
:Status: draft
					 
Un autre article mais sur PostgreSQL.

On peut prévoir des requêtes :

.. code-block:: sql
  
  SELECT 
    m.contrat_id,
    m.kb,
    count(l.amount_prosp) AS nb_affaire,
    coalesce(sum(l.amount_prosp::integer) FILTER (WHERE l.fk_c_status=6),0) AS total_revenus,
    m.total as nb_tickets,
    m.notes as nb_notes,
    COALESCE(EXTRACT (days FROM a.duration)/7*a.spp,0) AS estim_nb_cr_premium
		
  FROM llx_lead l
  JOIN karmin.llx_karmin_v_contracts_leads cl ON ( l.rowid = cl.fk_target )
  JOIN llx_contrat c   ON ( c.rowid = cl.fk_source)
  JOIN karmin.llx_karmin_v_mantis_tickets_per_account m ON (m.contrat_id= cl.fk_source)
  JOIN karmin.llx_karmin_v_accounts a ON (a.contrat_id = cl.fk_source)
	
  GROUP BY m.contrat_id,m.kb,m.total,m.notes,a.duration,a.spp
  ;

