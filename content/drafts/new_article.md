Title: un autre article
Date: 2018-05-26
Category: PostgreSQL
Lang: fr
Status: draft

Un autre article mais sur PostgreSQL.

On peut prévoir des requêtes :
   
    :::sql
    SELECT 
      m.contrat_id,
      m.kb,
      count(l.amount_prosp) AS nb_affaire,
      coalesce(sum(l.amount_prosp::integer) FILTER (WHERE l.fk_c_status=6),0) AS total_revenus,
      m.total as nb_tickets,
      m.notes as nb_notes,
      COALESCE(EXTRACT (days FROM a.duration)/7*a.spp,0) AS estim_nb_cr_premium
    FROM   
      llx_lead l
      JOIN karmin.llx_karmin_v_contracts_leads cl ON ( l.rowid = cl.fk_target )
      JOIN llx_contrat c   ON ( c.rowid = cl.fk_source)
      JOIN karmin.llx_karmin_v_mantis_tickets_per_account m ON (m.contrat_id= cl.fk_source)
      JOIN karmin.llx_karmin_v_accounts a ON (a.contrat_id = cl.fk_source)
    GROUP BY m.contrat_id,m.kb,m.total,m.notes,a.duration,a.spp
    ;
 

Test de table



| Tables        | Are           | Cool  |
| ------------- |:-------------:| -----:|
| col 3 is      | right-aligned | $1600 |
| col 2 is      | centered      |   $12 |
| zebra stripes | are neat      |    $1 |

Une autre

Markdown | Less | Pretty
--- | --- | ---
*Still* | `renders` | **nicely**
1 | 2 | 3



| 1 client | SQL Server (ms) | PostgreSQL (ms) | Rapport PostgreSQL / SQL Server | PostgreSQL plus rapide |
|:--:|:-----------:|:-----------:|:-----:|:-----:|
| Q1 |    333      |    435      | 131 % |       |
| Q2 |    412      |     52      |  13 % |  X    |
| Q3 |  2 730      | 12 824      | 470 % |       |
| Q4 |  1 498      |     14      | 0,9 % |  X    |
| Q5 |  3 765      | 16 199      | 430 % |       |
| Q6 |  1 933      |    376      |  19 % |  X    |
| Q7 |    112      |     33      |  29 % |  X    |
| Q8 |    592      |    115      |  20 % |  X    |
| Q9 |  1 393      |     81      | 5,9 % |  X    |
| Q10| 14 065      | 29 467      | 210 % |       |
| **Total** | **27 secondes** | **1 minute** |   | **6 / 10** |
